const { freeze } = Object;
const queryError = document.getElementById('query-error');
const queryField = document.getElementById('query');
const results = document.getElementById('results');

const FILE_TYPES = freeze({
  PDF: 'pdf',
  PPTX: 'pptx',
  // ...and other types
});

const getSvgAsset = (name) => `assets/${name}.svg`;

const FILE_TYPE_LOGO_MAP = Object.freeze({
  [FILE_TYPES.PDF]: getSvgAsset('pdf-icon'),
  [FILE_TYPES.PPTX]: getSvgAsset('pptx-icon'),
  // ...and for other types
});

const FILE_TYPE_URL_MAP = Object.freeze({
  [FILE_TYPES.PDF]: 'http://localhost:3000/parse',
  //.... and other urls
});

function setResults(files, fileType) {
  let html = '';

  for (const file of files) {
    html += `
      <li>
        <a class="file" href="${file.url}" target="_blank" rel="noopener noreferrer">
          <img src="${FILE_TYPE_LOGO_MAP[fileType]}" />
          ${file.name}
        </a>
      </li>`;
  }

  results.innerHTML = html;
}

const MOCKED_FILES = [
  { url: 'https://source.net/foo.pdf', name: 'FileName_Foo.pdf' },
  { url: 'https://source.net/bar.pdf', name: 'FileName_Bar.pdf' },
  { url: 'https://source.net/baz.pdf', name: 'FileName_Baz.pdf' },
];

async function searchFilesAsync(fileType, query) {
  const params = new URLSearchParams({ query });

  const url = FILE_TYPE_URL_MAP[fileType];

  if (!url) return;

  const response = await fetch(url + '?' + params, {
    mode: 'cors',
  });

  const data = await response.json();

  return data.files || [];
}

document.forms[0].addEventListener('submit', (e) => {
  e.preventDefault();

  const query = queryField.value.trim();

  if (!query) {
    queryField.classList.add('error');
    queryError.style.display = 'block';
    return;
  }

  const fileType = document.getElementById('type').value;

  results.style.display = 'flex';
  results.innerHTML = '<span class="loader"></span>';

  searchFilesAsync(fileType, query)
    .then((files) => {
      if (!files?.length) {
        results.innerHTML =
          '<li class="not-found">По вашому запиту нічого не знайдено :(</li>';
        return;
      }

      setResults(files, fileType);
    })
    .catch(console.error);
});

queryField.addEventListener('keydown', () => {
  queryField.classList.remove('error');
  queryError.style.display = 'none';
});
